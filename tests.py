# test class
from program_v8 import*

def testPolygonClass():

	p=Polygon()

	def testaddPoint():
		print 'testing addPoint...',
		assert p.pointList == []
		p.addPoint((1,1))
		assert p.pointList==[(1,1)]
		p.pointList.pop()
		assert p.pointList==[]
		print 'passed'

	def testUndoLastPoint():
		""" test function for undo last point"""
		print 'testing testUndoLastPoint..',
		p.pointList=[(0,0),(1,1)]
		p.undoLastPoint()
		assert (p.pointList==[(0,0)])
		p.undoLastPoint()

		assert(p.pointList==[])
		try:
			p.undoLastPoint()
		except:
			print 'failed for empty list'
		print 'passed'
	
	def testOrientOfLongestSide():pass

	def testLongestSide():
		"""test function for longest side"""
		print 'testing longestSide...'
		p.pointList=[(0,0),(1,1),(2,2),(3,3)]
		assert(p.longestSide()==[(3,3),(0,0)])
		p.pointList=[]
		assert(p.longestSide()==None)
		p.addPoint((1,1))
		assert(p.longestSide()==None)
		print 'passed'

	def testconvexCheck():
		print 'test convexCheck...',
		# bug - doesnt handle parallel cases

		# p.pointList=[(0,0),(1,1),(2,2)]
		# assert p.convexCheck()==True

		p.pointList=[]
		assert p.convexCheck()==True

		p.pointList= [(0,0),(2,2),(2,3)]
		assert p.convexCheck()==True

		p.pointList=[(0,0),(2,2),(2,1),(0,1)]
		assert p.convexCheck()==False


		p.pointList=[(1,1),(2,2),(3,1),(2,0),(0,1)]
		assert p.convexCheck()==True

		print 'passed'

	def testrotateAxis():
		print 'testing rotateAxis...',
		pi_degrees=180 
		radian45=45.0*math.pi/pi_degrees
		p.pointList=[]
		p.rotateAxis(radian45)
		assert p.pointList==[]

		p.pointList=[(1,0)]
		p.rotateAxis(radian45)
		print p.pointList

		p.pointList=[(1,0),(2,0)]
		p.rotateAxis(math.radians(45))
		print p.pointList

	def testgetLayoutOrigin():
		print 'testing getLayoutOrigin...',

		p.pointList=[(0,0),(1,1),(1,0)]
		assert p.getLayoutOrigin()==(1,1)

		p.pointList=[(1,1),(2,1),(2,2)]
		assert p.getLayoutOrigin()==(2,2)

		p.pointList=[(1,1),(1,3),(2,2)]
		assert p.getLayoutOrigin()==(1,3)

		print 'pass'

	def testgetLayoutOrientationDegrees():
		print 'testing getLayoutOrientationDegrees...',
		p.pointList=[(0,0),(1,1),(1,0)]
		assert round(p.getLayoutOrientationDegrees()-135.0)==0

		p.pointList=[(1,1),(1,2),(2,2)]
		assert round(p.getLayoutOrientationDegrees()+45.0)==0

		p.pointList=[(1,1),(1,3),(2,2)]
		assert round(p.getLayoutOrientationDegrees()- 90.0)==0

		print 'pass'

	def testpointInPolygon():
		p.pointList=[(0,0),(1,0),(1,2),(0,1)]
		print p.pointInPolygon((0.5,0.5))

		# handle cases where the 
		p.pointList=[(0,0),(1,0),(1,1),(0,1)]
		print p.pointInPolygon((3,0.5))

		p.pointList=[(0,0),(1,0),(1,1),(0,1)]
		print p.pointInPolygon((0.5,0))


	# testUndoLastPoint()
	# testrotateAxis()
	# testconvexCheck()
	# testgetLayoutOrigin()
	# testgetLayoutOrientationDegrees()
	testpointInPolygon()
	# testLongestSide()


def testCarParkLayoutClass():
	""" prints ot a dictionary of a carparklayout in readable formatted
	string"""

	def testPrintDict():
		testCarParkLayout= CarParkLayout(300,200)
		for key in sorted(testCarParkLayout.carPointDict):
			if (key[1] ==0):
				print
			print key,'->',testCarParkLayout.carPointDict[key],"\t",

	# also tests un transform

	def testtransformCarPointsForDisplay():

		layout=CarParkLayout(75,175, carWidth=25, carLength=50,
			orientation=90,originX=250,originY=250)
		print '*** for 6 cars in 3 car wide and 2 rows with middle'
		print '	lane configuration.. 0,0 at 250,250, orient =90 '
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		layout.transformCarPointsForDisplay()
		print 'after transformation'
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		print ' now untransform'
		layout.unTransformCarPointsForDisplay()
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		print '*********************************************************'


		layout=CarParkLayout(75,175, carWidth=25, carLength=50,
			orientation=135,originX=250,originY=250)
		print '*** for 6 cars in 3 car wide and 2 rows with middle '
		print 'lane configuration.. 0,0 at 250,250, orient =135 \n '
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		layout.transformCarPointsForDisplay()
		print 'after transformation'
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		print ' now untransform'
		layout.unTransformCarPointsForDisplay()
		for point in sorted(layout.carPointDict):
			print point,'->',layout.carPointDict[point]
		print '*********************************************************'


		for i in xrange(0,360,45):

			print 'i = %d degrees'%i

			layout=CarParkLayout(75,175, carWidth=25, carLength=50,
				orientation=i,originX=250,originY=250)
			print '*** for 6 cars in 3 car wide and 2 rows with middle '
			print 'lane configuration.. 0,0 at 250,250, orient =135 \n '
			for point in sorted(layout.carPointDict):
				print point,'->',layout.carPointDict[point]
			layout.transformCarPointsForDisplay()
			print 'after transformation'
			for point in sorted(layout.carPointDict):
				print point,'->',layout.carPointDict[point]
			print ' now untransform'
			layout.unTransformCarPointsForDisplay()
			for point in sorted(layout.carPointDict):
				print point,'->',layout.carPointDict[point]
			print '*********************************************************'


		pass

	testtransformCarPointsForDisplay()

def testRectangleCar():
	screen = pygame.display.set_mode((1024,768))
		# self.screen2=pygame.display.set_mode((width,height))
	pygame.display.set_caption('testRectangleCar')
	tempvar=True
	car=RectangleCar()
	car.addPoint((10,10))
	# print '10,10 point added', car.getDynamicPointList()
	while tempvar:
		pygame.display.flip()
		for event in pygame.event.get():
			# print 'pointList', car.pointList
			# print 'Dynamic List: ', car.getDynamicPointList()
			(x,y)=pygame.mouse.get_pos()
			# print ' current position : ',(x,y)
			if (event.type == pygame.QUIT):
				pygame.quit()
			elif (event.type == pygame.MOUSEBUTTONDOWN):
				car.addPoint((20,20))
			elif (event.type == pygame.KEYDOWN):
				pass

# testRectangleCar()
# testCarParkLayoutClass()
# testPolygonClass()



an=Anime('an')
an.run()

# print math.sin


def checkSpriteInBound(k,carLength,carWidth):
		""" takes in key {i.e (row,col)} of the carSpriteDict and checks if 
		four corners of car are in the polygon. returns false even if 
		one of them is not inside"""

		# we call the NE point as A the other points will be B,C,D
		[xA,yA,o]=k


		# compute B
		xB= xA + carLength*math.cos(math.radians(o - 90))
		yB= yA - carLength*math.sin(math.radians(o-90))

		#compute C
		diag= (carLength**2+carWidth**2)**0.5
		negAngle=math.degrees(math.atan(carLength/float(carWidth)))
		xC= xA + diag*math.cos(math.radians(o- negAngle))
		yC= yA - diag * math.sin(math.radians(o- negAngle))

		#compute D
		xD= xA + carWidth* math.cos(math.radians(o))
		yD= yA - carWidth* math.sin(math.radians(o))

		
		print xA,yA
		print xB,yB
		print xC,yC
		print xD,yD


		# listOfpoints=[(xA,yA),(xB,yB),(xC,yC),(xD,yD)]
		# for point in listOfpoints:
		# 	print point,
		# 	print self.boundary.pointInPolygon(point)
		# 	if not (self.boundary.pointInPolygon(point)):
		# 		print 'false returnned'
		# 		return False
		# return True

def testcheckSpriteInBound():

	checkSpriteInBound([100,100,0],50,20)
	print ' *****************'
	print 'orientation is 90 '
	checkSpriteInBound([100,100,90],50,20)
	print 'orientation is 180 '
	checkSpriteInBound([100,100,180],50,20)
	print 'orientation is 270'
	checkSpriteInBound([100,100,270],50,20)


# testcheckSpriteInBound()