# bismillah
import pygame
from pygame.locals import*
from pygame.examples import*

import math
import sys
import os

class Button(object):

	def __init__(self,s='m'):

		# the file path shuld be in the format filename.ext
		# filename_up.ext, filename_hover.ext
		# and all the three files should be present

		# self.filePathButtonUp=s
		# # 4 to remove and replace an extention.
		# self.filePathButtonDown=s[:-4]+'_dn'+s[-4:]
		# self.filePathButtonHover=s[:-4]+'_hover'+s[-4:]

		# # up , down and hover are three pygame surfaces
		# self.up=pygame.image.load(self.filePathButtonUp)
		# self.down=pygame.image.load(self.filePathButtonDown)
		# self.hover=pygame.image.load(self.filePathButtonHover)

		self.isActive=False # this controls the state of the button
		while (self.isActive): 
			self.animate() # this shall display and also detect hovers

	def animate(self):

		pass

	def isClicked(self,(x,y)):
		pass

class RectButton(Button):

	def __init__(self,left,top,right,bottom,s='m'):
		self.left=left
		self.top = top
		self.right=right
		self.bottom=bottom
		super(RectButton,self).__init__(s)

	def isClicked(self,(x,y)):
		if self.left<x<self.right and self.top<y<self.bottom:
			return True
		return False

class CircButton(Button):
	def __init__(self,center,radius,s='m'):
		(self.cx,self.cy),self.radius=center,radius
		self.left=self.cx-self.radius
		self.top=self.cy- self.radius
		self.bottom=self.cy+self.radius
		self.right=self.cx+ self.radius

		super(CircButton,self).__init__(s)

	def isClicked(self,(x,y)):
		# circle equation x**2 + y**2 < r**2
		return (x-self.cx)**2 + (y-self.cy)**2 < self.radius**2


######################################################



class Polygon(object):
	# class which holds point list and related methods 
	def __init__(self,close=False):
		self.pointList=[]
		self.closePoly=close# close is keyword optional arg.

	def addPoint(self,xyTuple):
		self.pointList.append(xyTuple)
		if not self.convexCheck():
			self.undoLastPoint()

	def __iter__(self):
		self.iterCount=0
		return self

	def next(self):
		self.iterCount+=1
		if self.iterCount>len(self.pointList): raise StopIteration	
		return self[self.iterCount-1]

	def __getitem__(self,key):
		return self.pointList[key]

	def undoLastPoint(self):
		# pop the last point from the list
		# condition not to pop from empty lists
		if len(self.pointList)>=1:
			self.pointList.pop()

	# returns orientation in radians
	def orientOfLongestSide(self):
		""" orientation of longest side provided in Radian"""
		""" uses normal co-ord system"""
		if len(self.pointList)<2:return None
		slope= self.getSlope(self.longestSide())
		return math.atan(slope)

	def longestSide(self):
		""" returns the longest side of the polygon as a two point list"""



		# empty lists check
		if len(self.pointList)<2:
			print 'No Sides in polygon'
			return None

		l=self.pointList
		maxDist=0
		lineSegment=[] # will be a list of two point tuples later
		for i in xrange(len(l)):
			dist= self.measureDist(l[i-1],l[i])
			if dist > maxDist:
				maxDist=dist
				lineSegment=[l[i-1],l[i]]
		return lineSegment

	def shortestSide(self):
		""" returns the shortest side of the polygon as a two point list"""

		# empty lists check
		if len(self.pointList)<2:
			return None

		l=self.pointList
		minDist=self.measureDist(l[-1],l[0])
		lineSegment=[l[-1],l[0]] 
		for i in xrange(len(l)):
			dist= self.measureDist(l[i-1],l[i])
			if dist < minDist:
				minDist=dist
				lineSegment=[l[i-1],l[i]]
		return lineSegment

	# tested ok.
	def convexCheck(self):
		""" this function is intended to check with concavity or convexit of
		the polygon being input. Will check at the time of input"""
		l= self.pointList

		# handle empty list, single point, single linesegment, triangle
		fourDescribedCases=4
		if len(l)<fourDescribedCases:
			return True

		#newest line segment is
		newLS= [l[::-1][1],l[::-1][0]]

		# check for all previous line segment
		for i in xrange(len(l)-2):
			# loop thru all the line segments in order of input
			# except the newest LS. index len(l)-1 ,i.e newest point
			# should not be reached.( )

			LS=[l[i],l[i+1]]
			
			# intersection of LS and newLS

			interS=self.intersectionOfLines(LS,newLS)

			if interS==None : return False

			# check if intersection point falls in the range of 
			# both of the line segments. I choose to check with newLS.
			if (min(newLS[0][0],newLS[1][0])<interS[0]<max(newLS[0][0],
				newLS[1][0])) and (min(LS[0][0],LS[1][0])<interS[0]<\
				max(LS[0][0],LS[1][0])):
				return False

		return True

	# returns slope of a lineSegment
	# manages infinite value using math.atan(Pi/2)
	def getSlope(self,lineSegment):
		""" returns the slope of line segment"""
		p1,p2=lineSegment[0],lineSegment[1]

		# handle zero division error, return infinite slope 
		if (float(p2[0])-p1[0])==0:
			return math.tan(math.pi/2.0)

		return (p2[1]-p1[1])/(float(p2[0])-p1[0])

	# measures distance between two points 
	# accessed as instance method
	def measureDist(self,pointTuple1,pointTuple2):
		""" returns distance between two points """
		return ((pointTuple1[0]-pointTuple2[0])**2 + 
			(pointTuple1[1]-pointTuple2[1])**2)**0.5

	# gets the max x cordinate among the pointlist
	# returns single int value
	def getMaxX(self):
		""" finds the max X value to define extents"""
		if len(self.pointList)<1:
			return None

		maxX=self.pointList[0][0]
		for point in self.pointList:
			if point[0]>maxX:
				maxX=point[0]

		return maxX

	# gets the min X cordinate among the pointlist
	# returns single int value
	def getMinX(self):
		""" finds the min X value to define extents"""
		if len(self.pointList)<1:
			return None

		minX=self.pointList[0][0]
		for point in self.pointList:
			if point[0]<minX:
				minX=point[0]
		return minX

	# gets the max Y cordinate among the pointlist
	# returns single int value
	def getMaxY(self):
		""" finds the max Y value to define extents"""
		if len(self.pointList)<1:
			return None

		maxY=self.pointList[0][1]
		for point in self.pointList:
			if point[1]>maxY:
				maxY=point[1]

		return maxY

	# gets the min Y cordinate among the pointlist
	# returns single int value
	def getMinY(self):
		""" finds the min Y value to define extents"""
		if len(self.pointList)<1:
			return None

		minY=self.pointList[0][1]
		for point in self.pointList:
			if point[1]<minY:
				minY=point[1]

		return minY

	# has been later decided that this function shall not be used.
	def distanceFromPointToLine(self,lineSegment,point):
		""" calculates the perpendicular distance from a point to 
		a line """
		slope= self.getSlope(lineSegment)
		slopeOfPerpendicular=-1/slope

		# getanother point based on the slope of perpendicular
		point2=(0.0,point[1]-slopeOfPerpendicular*point[0])

		footOfPerp= self.intersectionOfLines(lineSegment,[point,point2])
		return self.measureDist(footOfPerp,point)

	# returns intersection points of two line segments
	# called as a instance method.
	def intersectionOfLines(self,lineSegment1,lineSegment2):
		""" takes two linesegments as [(x1,y1),(x2,y2)] and 
		returns intersection as a point (x,y). 
		returns None for parallel lines."""

		[(x1,y1),(x2,y2)]=lineSegment1
		[(x3,y3),(x4,y4)]=lineSegment2
		# vertical lines
		if (x1==x2 and x3==x4):
			return None

		elif(x1==x2):
			a2 = (y4-y3)/float(x4-x3)
			b2 = y3 - a2*float(x3)
			# y=a2 x + b2 is the non vertical line
			#@ x=x1  
			return [x1,float(a2*x1 + b2)]

		elif (x3==x4):
			a1 = (y2-y1)/float(x2-x1)
			b1 = y1 - a1*float(x1)

			return [x3,float(a1*x3 + b1)]

		else:
			a1 = (y2-y1)/float(x2-x1)
			b1 = y1 - a1*float(x1) 
			a2 = (y4-y3)/float(x4-x3)
			b2 = y3 - a2*float(x3)
		if a1==a2 : return None

		x0 = -(b1-b2)/(a1-a2)
		y0= a1*x0 + b1
		return (x0,y0)

	# rotates points of point list considering Y-up co-ordinate system
	# and acw positive.
	def rotatePoints(self,angle):pass

	# angle to be provided in radian
	# 1 radian is not pi. 3.14 radian is pi.
	# destructively modifies the pointList.
	def rotateAxis(self,angle):
		l=self.pointList
		newPointList=[]
		if len(l)<1:return []

		for point in l:
			x,y=point[0],point[1]
			X=x*math.cos(angle) + y*math.sin(angle)
			Y=-x*math.sin(angle) + y*math.cos(angle)
			newPointList.append((X,Y))

		self.pointList=newPointList

	# layout origin should be one of the vertices of longest side
	# this function tells exactly which one.
	def getLayoutOrigin(self):
		# code for case where longest side is vertial, both x are same

		if len(self.pointList)<2:
			assert False
			return (0,0)# handle the empty list by returning (0,0)
			# returning none might raise errors


		LS=self.longestSide()

		# get index of second point of longest side
		index=0
		for i in xrange(len(self.pointList)):
			if self.pointList[i]==LS[1]:
				index=i

		# if y co-ordinate of next point is greater , then the polygon
		# lies below the longest side. the return first point of LS
		j=1
		while True: # while loop to accomodate colinear horizontal points
			k=(index+j)%len(self.pointList)
			if self.pointList[k][1]>LS[1][1]:
				# return point with lesser x; for same x- greater y
				# use sorted
				return sorted(LS)[0]
			else:
				return sorted(LS)[1]

	# returns Layout orientation in degrees.
	# to be directly fed into pygame.surface.rotate
	def getLayoutOrientationDegrees(self):

		if len(self.pointList)<2:
			assert False
			return 0# handle the empty list by returning (0,0)
			# returning none might raise errors

		pi_degrees=180
		LS=self.longestSide() # shall be -ve for anti clockwise
		angle_rad=self.orientOfLongestSide()
		angle_deg=angle_rad*pi_degrees/math.pi

		# get index of second point of longest side
		index=0
		for i in xrange(len(self.pointList)):
			if self.pointList[i]==LS[1]:
				index=i

		# if y co-ordinate of next point is greater , then the polygon
		# lies below the longest side. the return first point of LS
		j=1
		while True: # while loop to accomodate colinear points
			k=(index+j)%len(self.pointList)
			if self.pointList[index+j][1]>LS[1][1]:
				# return angle as is  for -ve it will be -ve
				# for +ve it will be +ve
				return -angle_deg				
			else:
				#return 180 + angle
				return -angle_deg+pi_degrees

	def pointInPolygon(self,point):
		# shuld deal with point intersection counting as once
		# vertice shuld be counted seperately and added

		""" checks and returns True if point is in polygon
			points on the edges are also returned True"""

		# get point list and max X
		l=self.pointList

		maxX,minX =self.getMaxX(),self.getMinX()
		maxY,minY=self.getMaxY(),self.getMinY()


		if point[0]>maxX or point[0]<minX or point[1]<minY or point[1]>maxY:
			return False

		# lets choose y=c line segment passing thru the point
		# i.e y=point[1], draw a line segment till infinite
		# here we do till max X
		LSthruPoint=[point,(maxX,point[1])]

		cutCounter=0

		# get all lines in the polygon 
		for i in xrange(-1,len(l)-1):
			side=l[i],l[i+1]
			# get intersection point
			iP=self.intersectionOfLines(LSthruPoint,side)
			

			if iP==None: continue # parallel lines

			# check for intersection point on both the infinite line segment
			# and also the polygon.
			# this gives the directionality necessary!!

			if (iP[0]>=min(side[0][0],side[1][0]) and \
				iP[0]<=max(side[0][0],side[1][0])) and \
				(iP[0]<=max(maxX,point[0]) and iP[0]>= min(maxX,point[0])):
				
				cutCounter+=1

		return cutCounter%2==1

	
class RectangleCar(Polygon):
	def __init__(self):
		super(RectangleCar,self).__init__(True)
		self.carWidthLengthRatio=2 # length is 2 times width

	# adds points cautiouslysuch that points list holds
	# only 0, 1 ,2 or 4 points and makes a rectangle out of it.
	def addPoint(self,(x,y)):
		if len(self.pointList)>=3:
			return
		elif len(self.pointList)==2:
			#use the same dynamic rectangle function but this 
			# time assign the list to our pointlist which signifies
			# the rectangle
			self.pointList=self.makeDynamicConstrainedRectangle((x,y))
		else:
			# print ' point list from the class', self.pointList
			super(RectangleCar,self).addPoint((x,y))

	# returns points based on the mouse position 
	# could adopt a similar method to even draw polygon.
	# do if time permits
	def getDynamicPointList(self):
		""" returns the point list dynamicaly modified by mouse position"""
		(x,y)= pygame.mouse.get_pos()

		if len(self.pointList)==4:
			return self.pointList
		elif len(self.pointList)==3:
			assert False
		elif len(self.pointList)==2:
			return self.makeDynamicConstrainedRectangle((x,y))

		elif len(self.pointList)==1:
			return self.pointList+[(x,y)]

	#this could have been better constructed by giving only 8 options
	def makeDynamicConstrainedRectangle(self,(x,y)):
		""" only when there are two points in the point list"""

		rtX,rtY=x,y

		x1,y1=(self.pointList[0][0],self.pointList[0][1])
		x2,y2=(self.pointList[1][0],self.pointList[1][1])

		slope=self.getSlope([(x1,y1),(x2,y2)])
		
		slopeOfPerp=-1/slope

		# get a point on a parallel line passing thru (x,y)
		point2=(0.0,rtY-slope*rtX)

		#get a point on a perpendicular line segment of self.pointList[0]
		point3=(0.0,y1-x1*slopeOfPerp)

		#get a point on a perpendicular line segment of self.pointList[1]
		point4=(0.0,y2-x2*slopeOfPerp)

		lineSegmentParallel=[(rtX,rtY),point2]

		lineSegmentPerp1=[(x1,y1),point3]
		lineSegmentPerp2=[(x2,y2),point4]

		(x3,y3)=self.intersectionOfLines(lineSegmentPerp1,lineSegmentParallel)
		(x4,y4)=self.intersectionOfLines(lineSegmentPerp2,lineSegmentParallel)

		#make a return List
		rList=[]
		rList+=self.pointList
		rList.append((x4,y4))
		rList.append((x3,y3))
		

		return rList

	# gets shorter side measure of the rect. 
	def getCarWidth(self):
		"""returns the distance of smaller edge of rectangles"""
		if len(self.pointList)<2:
			return 0
		distMin=self.measureDist(self.pointList[0],self.pointList[1])
		for i in xrange(len(self.pointList)):
			for j in xrange(i+1,len(self.pointList)):
				dist=self.measureDist(self.pointList[i],self.pointList[j])
				if dist<distMin:
					distMin=dist
		return distMin

	# gets the measure of longest side of car.
	def getCarLength(self):
		#get car length line
		distMax=0
		for i in xrange(len(self.pointList)):
			for j in xrange(i+1,len(self.pointList)):
				dist=self.measureDist(self.pointList[i],self.pointList[j])
				if dist>distMax:
					distMax=dist
		return distMax

	# dynamically undoes the last point. takes care so that there 
	# will never be three points and triangle formed
	def undoLastPoint(self):

		if len(self.pointList)>4:
			assert False

		if len(self.pointList)==4:
			# when undoing from 4 points it goes to two points
			self.pointList.pop()
			self.pointList.pop()

		elif len(self.pointList)==3:
			assert False

		elif len(self.pointList)==2:

			self.pointList.pop()

		elif len(self.pointList)==1:
			return self.pointList.pop()


class CarParkLayout(object):
	"""this is a container class for all points where there are cars in 
	carpark layout. all such points are kept as point tuples"""

	def __init__(self,maxWidth,maxHeight,orientation=0,
		carWidth=2.5,carLength=5,originX=0,originY=0):

		pi_degrees=180

		# width is the orientation the cars width is oriented
		self.maxWidth=maxWidth

		self.maxHeight=maxHeight
		self.orientationRad=float(orientation)/pi_degrees*math.pi
		self.orientationDeg=orientation
		self.carWidth=carWidth
		self.carLength=carLength
		self.carPointDict=dict()
		self.carSpriteDict=dict()
		self.layoutSpriteGrp=pygame.sprite.Group()
		

		# road width is defined by carWidth. but could be better a 
		# function of carWidth and orientation angle. 
		# check neufert standards for car design and compute a
		# function if time permits
		self.roadWidth = self.computeRoadWidth() #this can be put in betterfn.
		self.originX=originX
		self.originY=originY
		self.computeCarPointDict()
		self.makeCarSpriteDict()


		# ditching the method and doing all over using rects...
		# self.magicallyDoWorkForME()
		# will use a polygon directly.. and output a dictionary 
		# of rects and orientation of surface rotation.

	# could improve this function.
	def computeRoadWidth(self):
		return self.carWidth

	# creates a car point dict based on input parameters
	def computeCarPointDict(self):
		"""a carPoint shall be a NW corner point w.r.t car and 
		orientation setup in (X, Y, angle) manner. the angle will
		 define how the car is oriented w.r.t the layout.
		 angle is in degrees.

		"""

		# orientation at this moment is kept at 90 or 270 degrees based on 
		# presence of how it attaches to the road.
		orient180=180+self.orientationDeg
		orient0=0+self.orientationDeg
		oWhole=self.orientationRad
		
		self.numOfCols=int(self.maxWidth/self.carWidth)
		numOfCols=self.numOfCols

		# height of the 
		# configuration below is taken ( % is car )and
		# %%%%%
		# -----
		# %%%%%
		# which is 2 cars + road 
		self.numOfRows=2*int(self.maxHeight/(self.carLength*2+self.roadWidth))
		numOfRows=self.numOfRows
		emptySpace= self.maxHeight % (self.carLength*2+self.roadWidth)

		if emptySpace> self.roadWidth+self.carLength:
			numOfRows+=1

		# populate the carpark dict
		# dict with have row col tuple as key 
		# and its carPoint i.e ( X,Y,orientation) as value
		for i in xrange(numOfRows):

			if i%2==0 : orient = orient0
			else: orient=orient180
			for j in xrange(numOfCols):
				# x1 y1 are the points on the display
				# y1 is intentionaly inverted to keep the y axis 
				# directionality
				x1=(j+ i%2)*self.carWidth
				
				y1=-(self.carLength*(i + i%2)+((i+1)/2)*self.roadWidth)
				

				# rotation calculation
				# rotation of y axis is inverted back to get the 
				# coordinate point on our display
				x1t=x1*math.cos(oWhole)-y1*math.sin(oWhole)
				y1t=-(x1*math.sin(oWhole)+y1*math.cos(oWhole))
				self.carPointDict[(i,j)]=(self.originX+x1t,
					self.originY+y1t,orient)

	def convertPointRotToRect(self,point,carWidth,carHeight,rotDeg):
		rotDeg%=360

		rotRad=math.radians(rotDeg)


		surf=pygame.Surface([carWidth,carHeight])
		surf=pygame.transform.rotate(surf,rotDeg)
		rec=surf.get_rect()


		(recWidth,recHeight)=rec.width,rec.height

		#case1: rot=0
		if rotDeg==0:
			left=point[0]
			top=point[1]
		elif (0<rotDeg and rotDeg<90):
			left= point[0]
			top=point[1]- carWidth*math.sin(rotRad)
			# case 1 and 2 can be merged becasuse sin 0 = 0
		elif rotDeg==90:
			left= point[0]
			top=point[1]-carWidth
			# actually this can also be merged with the above

		elif rotDeg>90 and rotDeg<180:
			left=point[0] - carWidth* math.cos(math.pi - rotRad)
			top = point[1] - recHeight

		elif rotDeg==180:
			left = point[0] -recWidth
			top = point[1] - recHeight

		elif (rotDeg>180 and rotDeg<270):
			left = point[0] - recWidth
			top = point[1] - carHeight* math.cos(rotRad - math.pi)

		elif rotDeg==270:
			left = point[0] - recWidth
			top = point[1]
		elif (rotDeg>270 and rotDeg<360):
			left=point[0]-carHeight*math.cos(rotRad - math.pi -math.pi/2)
			top = point[1]
		else:
			assert False

		rRect=Rect(left,top,recWidth,recHeight)

		if rotDeg==0:
			print ' rRect = ',rRect
		return rRect

	def makeCarSpriteDict(self):
		""" returns a dictionary of car sprites """

		f=self.convertPointRotToRect

		class CarSprite(pygame.sprite.Sprite):

			def __init__(self,oPoint, width=20, height=50,rotation=0):

				# Call the parent class  constructor
				super(CarSprite,self).__init__()

				# This is an image loaded from the disk.
				# shuld be replaced with appropriate input into the function
				self.image = pygame.image.load("UI_Imgs/carImg.jpg").convert()
				self.image.set_colorkey((0,0,0))
				self.image= pygame.transform.scale(self.image,
					(int(width),int(height)))
				self.image = pygame.transform.rotate(self.image,rotation)

				#get the rect from conversion function
				self.rect = f(oPoint,width,	height,rotation)
				self.hitmask=pygame.surfarray.array_colorkey(self.image)

		d=self.carPointDict

		dSp=self.carSpriteDict
		lsg= self.layoutSpriteGrp

		for key in sorted(d):
			[X,Y,o] =d[key]
			oPoint=(X,Y)
			width=self.carWidth
			height=self.carLength
			rot=o
			dSp[key]=CarSprite(oPoint,width,height,rot)
			lsg.add(dSp[key])


	# yes, hello. really . just to ask you how you are 
	# doing. and make you feel fine.!!!
	def hello():
		print ' you will love your code. work hard and finish it!!'
		pass


class Anime(object):

	# class init :)
	def __init__(self,title):
		self.title = title


	def redrawAll(self):

		# self.screen.blit(self.homeScreenBg,(0,0))
		if self.screenMode=='homeScreen':
			self.screen.blit(self.homeScreenBg,(0,0))
			self.screen.blit(self.qBup,(959,25))
			

		elif self.screenMode=='startScreen':
			self.displayStartScreen()
			


		# the polygon is drawn here.. 
		# based on x,y position of cursor there would be a line 
		# connecting to cursor and last point if the polygon is 
		# not completed
		elif self.screenMode=='drawScreen':

			self.displayDrawScreen()
			


			
		elif self.screenMode=='carRectInputScreen':
			self.displayCarDrawScreen()

			
			
		elif self.screenMode== 'optionScreen':
			self.screen.blit(self.opScreenBg,(0,0))

		elif self.screenMode=='helpScreen':
			k=self.currenthelpScrIndex
			self.screen.blit(self.refImage,(0,0))
			self.screen.blit(self.helpScreenHeader,(0,0))
			scr=self.helpScrList[k]
			# position(81,110) obtained for visual appeal..
			self.screen.blit(scr,(81,110))


		
		elif self.screenMode== 'debug_carParkLayout':pass
			
		elif self.screenMode== 'layoutGeneratedScreen':

			# variables for actual test

			surf=pygame.Surface([1024,768])
			surf.fill((0,0,0))
			surf.set_alpha(50)
			self.screen.blit(surf,(0,0))


			self.screen.blit(self.refImage,(0,0))
			self.screen.blit(self.layoutGeneratedScreenHeader,(0,0))

			cpl=self.layout

			d=self.layout.carSpriteDict

			cols=self.layout.numOfCols

			# the  middle column is set to be removed along with 
			# col no. 2 and 9 
			# this list is completely amendable based on user input.

			self.removeColList.append(cols/2)
			
			drawSpriteGrp=pygame.sprite.Group()
			
			self.blitBoundary()


			class BoundarySprite(pygame.sprite.Sprite):
				def __init__(self,boundary):

					super(BoundarySprite,self).__init__()
					self.boundary=boundary

					self.image=pygame.Surface((1024,768))
					self.image.set_colorkey((0,0,0))

					if not self.boundary.closePoly:
						(x,y)=pygame.mouse.get_pos()
						pointList=self.boundary.pointList +[[x,y]]
					else:
						pointList=self.boundary.pointList

					if len(pointList)>1:
						pygame.draw.aalines(self.image, (255,0,0), 
							self.boundary.closePoly , pointList, 1)

					self.rect=Rect(0,0,1024,768)
					self.hitmask=pygame.surfarray.array_colorkey(self.image)

			boundSprite=BoundarySprite(self.boundary)
			boundarySpGrp=pygame.sprite.GroupSingle(boundSprite)
			
			self.carCount=0


			for key in sorted(d):
				x,y=d[key].rect.centerx,d[key].rect.centery
				blankSurf=pygame.Surface([1024,768])
				pygame.sprite.Group(d[key]).draw(blankSurf)
				
				if self.boundary.pointInPolygon((x,y)) and \
					not key[1] in self.removeColList and\
					 self.checkSpriteInBound(key):
					self.carCount+=1
					
					drawSpriteGrp.add(d[key])



			drawSpriteGrp.draw(self.screen)
			s= '# of cars in Layout = '+str(self.carCount)
			
			tsurf= pygame.font.Font(None,30).render(s,False,(255,255,255))

			bSurf=pygame.Surface([300,100])
			bSurf.set_alpha(150)

			bSurf.blit(tsurf,(15,15))
			self.screen.blit(bSurf,(724,668))

			
		pygame.display.flip()

	def displayStartScreen(self):
		surf=pygame.Surface([1024,768])
		surf.fill((0,0,0))
		surf.set_alpha(50)
		self.screen.blit(surf,(0,0))

		self.screen.blit(self.refImage,(0,0))

		self.screen.blit(self.startScreenHeader,(0,0))

	def displayDrawScreen(self):
		surf=pygame.Surface([1024,768])
		surf.fill((0,0,0))
		surf.set_alpha(50)
		self.screen.blit(surf,(0,0))

		self.screen.blit(self.refImage,(0,0))

		self.screen.blit(self.drawScreenHeader,(0,0))

			# bound surf is a surface whre the polygon is drawn, 
			# this will be blitted in redraw all over the 
			# self.drawScreen surface which will be then 
			# flipped onto display.
			
		self.blitBoundary()

	def displayCarDrawScreen(self):

		self.screen.blit(self.refImage,(0,0))
		self.screen.blit(self.carDrawScreenHeader,(0,0))

		# carRect surf is a surface whre the polygon is drawn, 
		# this will be blitted in redraw all over the 
		# self.drawScreen surface which will be then 
		# flipped onto display. 
		
		# temporary surface - will die after this func.
		# next call a new surf is created an polygon is reblitted
		carRectSurf=pygame.Surface((1024,768))
		carRectSurf.set_colorkey((0,0,0))

		pointList=self.carPoly.getDynamicPointList()

		if pointList:
			pygame.draw.aalines(carRectSurf, (255,255,255), 
				self.carPoly.closePoly ,pointList, 1)
			self.screen.blit(carRectSurf,(0,0))


	def blitBoundary(self):
		""" helper function to blit boundary on screen """

		# create a surface
		self.boundSurf=pygame.Surface((1024,768))
		self.boundSurf.set_colorkey((0,0,0))


		if not self.boundary.closePoly:
			(x,y)=pygame.mouse.get_pos()
			pointList=self.boundary.pointList +[[x,y]]
		else:
			pointList=self.boundary.pointList

		if len(pointList)>1:
			pygame.draw.aalines(self.boundSurf, (255,255,255), 
				self.boundary.closePoly , pointList, 1)
		
		self.screen.blit(self.boundSurf,(0,0))

	# loads images	
	def loadImages(self):
		self.loadHomeScreenImages()
		self.loadHelpScreenImages()
		self.loadStartScreenImages()
		self.loadDrawScreenImages()
		self.loadCarDrawScreenImages()
		self.loadLayoutGeneratedScreenImages()

		self.refImageList=[]
		listOfFilenames= listFiles('Underlays')
		for filename in listOfFilenames:
			if filename[-4:]=='.jpg' or filename[-4:]=='.png':
				self.refImageList.append(pygame.image.load(filename).convert())

		self.refImage= self.refImageList[0]
		self.currentRefImageIndex=0

		

		self.opScreenBg=pygame.image.load("UI_Imgs/optionScreen.jpg"\
			).convert()

		self.carImg= pygame.image.load("UI_Imgs/carImg.jpg").convert()
		self.carImg.set_colorkey((0,0,0))

		self.drawScreen= pygame.image.load("UI_Imgs/drawPolygonScreen.jpg"\
			).convert()

		self.carRectInputScreen= pygame.image.load("UI_Imgs/inputCarRect.jpg"\
			).convert()

		


		self.loadHelpScreenImages()

	# helper for loadImages(self)
	def loadHomeScreenImages(self):
		# home screen background
		self.homeScreenBg = pygame.image.load("UI_Imgs/homeScreen.jpg"\
			).convert()

		# quit button
		self.qBPress=pygame.image.load("UI_Imgs/quitButton.jpg").convert()
		self.qBPress.set_colorkey((255,255,255))

		self.qBup=pygame.image.load("UI_Imgs/quitButton_up.jpg"\
			).convert()
		self.qBup.set_colorkey((255,255,255))

	def loadHelpScreenImages(self):

		self.helpScr1=pygame.image.load("UI_Imgs/helpScr1.jpg").convert()
		self.helpScr2=pygame.image.load("UI_Imgs/helpScr2.jpg").convert()
		self.helpScr3=pygame.image.load("UI_Imgs/helpScr3.jpg").convert()

		self.helpScr1.set_alpha(230)
		self.helpScr2.set_alpha(230)
		self.helpScr3.set_alpha(230)

		self.helpScreenHeader=pygame.image.load(\
			"UI_Imgs/topBannerForHelpScreen.jpg").convert()
		self.helpScreenHeader.set_alpha(180)
		self.helpScrList=[self.helpScr1,self.helpScr2,self.helpScr3]
		self.currenthelpScrIndex=0

		# load a help exit button.

	def loadStartScreenImages(self):
		pass
		self.startScreen = pygame.image.load(\
			"UI_Imgs/sampleParkingLotInt.jpg").convert()


		self.startScreen = pygame.Surface([1024,768])
		# fill it with out neutral yellow background
		self.startScreen.fill((249,237,232))

		self.startScreenHeader=pygame.image.load(\
			"UI_Imgs/topBannerForStartScreen.jpg").convert()
		self.startScreenHeader.set_alpha(180)

	def loadDrawScreenImages(self):
		self.drawScreenHeader=pygame.image.load(\
			"UI_Imgs/topBannerForDrawScreen.jpg").convert()

		self.drawScreenHeader.set_alpha(180)

	def loadCarDrawScreenImages(self):
		self.carDrawScreenHeader=pygame.image.load(\
			"UI_Imgs/topBannerForCarDrawScreen.jpg").convert()

		self.carDrawScreenHeader.set_alpha(180)

	def loadLayoutGeneratedScreenImages(self):
		self.layoutGeneratedScreenHeader=pygame.image.load(\
			"UI_Imgs/topBannerForLayoutGeneratedScreen.jpg").convert()
		self.layoutGeneratedScreenHeader.set_alpha(180)

	def dataInit(self):
		self.initButtons()
		self.screenMode='homeScreen'
		self.boundary= Polygon()
		self.carPoly=RectangleCar()
		self.removeColList=[2,9]
		self.clock=pygame.time.Clock()
		pass

	# initializes all buttons. specifically sets their
	# coordinate values here.
	def initButtons(self):

		# buttons for screen mode = homeScreen
		qBTop=25
		qBLeft=979-20
		qBBottom=65
		qBRight=979+20
		self.qB=RectButton(qBLeft,qBTop,qBRight,qBBottom)

		stBLeft=80
		stBTop=535
		stBRight=80+151
		stBbottom=584
		self.stB=RectButton(stBLeft,stBTop,stBRight,stBbottom)

		opBLeft=277
		opBTop=535
		opBRight=428
		opBbottom=584
		self.opB=RectButton(opBLeft,opBTop,opBRight,opBbottom)

		# buttons for screen mode = optionScreen
		bkBLeft= 80
		bkBTop = 150
		bkBRight= 80+ 151 # 151 is width of button
		bkBBottom= 150+ 49 # 49 is height of the button
		self.bkB=RectButton(bkBLeft,bkBTop,bkBRight,bkBBottom)

		# buttons for screen mode = startScreen

		radius=30
		hmBX,hmBY = 75,55
		self.loadImageButton=CircButton((hmBX,hmBY),radius)

		radius=30
		hmBX,hmBY = 969,55
		self.homeButton=CircButton((hmBX,hmBY),radius)

		radius = 30
		cX,cY = 150,55
		self.drawButton=CircButton((cX,cY),radius)

		radius = 30
		cX,cY = 225,55
		self.drawCarButton=CircButton((cX,cY),radius)

		radius = 30
		cX,cY = 815,55
		self.computeButton=CircButton((cX,cY),radius)

		radius = 30
		cX,cY = 892,55
		self.helpButton=CircButton((cX,cY),radius)




		# buttons for screen mode = drawScreen
		radius = 30
		cX,cY= 969,55
		self.helpButton_drawScreen=CircButton((cX,cY),radius)

		radius = 30
		cX,cY= 825,55
		self.undoButton_drawScreen=CircButton((cX,cY),radius)

		radius = 30
		cX,cY= 899,55
		self.discardButton_drawScreen=CircButton((cX,cY),radius)

		radius = 30
		cX,cY= 150,55
		self.backButton_drawScreen=CircButton((cX,cY),radius)



	# called at 20 fps. all event detection and redirection
	# handled here
	def timerFired(self):
		
		for event in pygame.event.get():

			if event.type == pygame.KEYDOWN:
				self.keyPressed()
			elif event.type == pygame.MOUSEBUTTONDOWN:
				self.mousePressed()
			elif event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()

		self.doHoverActions()

		
		self.clock.tick(20)
		self.redrawAll()

	def doHoverActions(self):
		(x,y)=pygame.mouse.get_pos()

		if self.screenMode=='drawScreen':

			if len(self.boundary.pointList)>0 :
				# dummy button at the start point of polygon
				radius = 15 # rough threshhold close to the start of polygon
				cX= self.boundary.pointList[0][0]
				cY= self.boundary.pointList[0][1]
				
				dummyButton=CircButton((cX,cY),radius)
				if dummyButton.isClicked((x,y)):
					pass
					# cursor = pygame.cursors.compile(pygame.cursors.broken_x)
					# pygame.mouse.set_cursor(*cursor)
					# pygame.cursors.tri_left

					
		

	def checkSpriteInBound(self,key):
		""" takes in key {i.e (row,col)} of the carSpriteDict and checks if 
		four corners of car are in the polygon. returns false even if 
		one of them is not inside"""

		# we call the NE point as A the other points will be B,C,D
		[xA,yA,o]= self.layout.carPointDict[key]
		

		carLength=self.carPoly.getCarLength()
		carWidth=self.carPoly.getCarWidth()



		# compute B
		xB= xA + carLength*math.cos(math.radians(o - 90))
		yB= yA - carLength*math.sin(math.radians(o-90))

		#compute C
		diag= (carLength**2+carWidth**2)**0.5
		negAngle=math.degrees(math.atan(carLength/float(carWidth)))
		xC= xA + diag*math.cos(math.radians(o- negAngle))
		yC= yA - diag * math.sin(math.radians(o- negAngle))

		#compute D
		xD= xA + carWidth* math.cos(math.radians(o))
		yD= yA - carWidth* math.sin(math.radians(o))
		# return True

		# exception for first row
		listOfpoints=[(xA,yA),(xB,yB),(xC,yC),(xD,yD)] if key[0]>0 else\
			[(xB,yB),(xC,yC)]

		for point in listOfpoints:
			# print point,
			# print self.boundary.pointInPolygon(point)
			if not (self.boundary.pointInPolygon(point)):
				return False
		return True

	
	# key pressed events handled here.
	def keyPressed(self):
		keyP=pygame.key.get_pressed()

		if keyP[K_ESCAPE]:
			pass
			# pygame.quit()
			# sys.exit()

		

	# mouse pressed events handled here
	def mousePressed(self):
		""" is called when mouse button is pressed"""

		# mouse position obtained
		(x,y)=pygame.mouse.get_pos()
		
		# when in home screen
		if self.screenMode=='homeScreen':
			# quit button blicked
			if self.qB.isClicked((x,y)):
				#a snap of qButton when pressed is blitted
				self.screen.blit(self.qBPress,(959,25)) 
				pygame.display.flip()
				pygame.quit()
				sys.exit()

			elif self.stB.isClicked((x,y)):
				#'start clicked'
				self.screenMode='startScreen'

			elif self.opB.isClicked((x,y)):
				#'options clicked'
				self.screenMode= 'optionScreen'

		elif self.screenMode == 'optionScreen':
			if self.bkB.isClicked((x,y)):
				self.screenMode= 'homeScreen'

		elif self.screenMode == 'startScreen':

			if self.loadImageButton.isClicked((x,y)):
				
				index=self.currentRefImageIndex
				index+=1
				index%=len(self.refImageList)
				self.refImage=self.refImageList[index]
				self.currentRefImageIndex=index
				pass
			
			elif self.drawButton.isClicked((x,y)):
				self.screenMode= 'drawScreen'

			elif self.drawCarButton.isClicked((x,y)):
				self.screenMode= 'carRectInputScreen'
			
			elif self.computeButton.isClicked((x,y)):
				if not(len(self.boundary.pointList)<3 or \
					len(self.carPoly.pointList)<4):
					self.generateDesign()
					self.screenMode='layoutGeneratedScreen'
				else:
					print 'no polygon / car rectangle input'

			elif self.helpButton.isClicked((x,y)):
				self.beforeHelpScreenMode=self.screenMode
				self.screenMode= 'helpScreen'

			elif self.homeButton.isClicked((x,y)):
				self.screenMode= 'homeScreen'

		elif self.screenMode == 'drawScreen':

			if len(self.boundary.pointList)>0 :
				# dummy button at the start point of polygon
				radius = 15 # rough threshhold close to the start of polygon
				cX= self.boundary.pointList[0][0]
				cY= self.boundary.pointList[0][1]
				
				dummyButton=CircButton((cX,cY),radius)
				if dummyButton.isClicked((x,y)):
					
					self.boundary.closePoly=True
					
					return
			

			if self.backButton_drawScreen.isClicked((x,y)):
				print ' backbutton presed'
				self.screenMode= 'startScreen'

			elif self.helpButton_drawScreen.isClicked((x,y)):
				self.beforeHelpScreenMode=self.screenMode
				self.screenMode='helpScreen'

			elif self.undoButton_drawScreen.isClicked((x,y)):
				self.boundary.closePoly=False
				self.boundary.undoLastPoint()

			elif self.discardButton_drawScreen.isClicked((x,y)):
				self.boundary.pointList=[]
				self.boundary.closePoly=False

			elif self.boundary.closePoly==False:
				self.boundary.addPoint((x,y))

				# in a range of 20 pixels of the 
			

		elif self.screenMode == 'carRectInputScreen':


			if self.backButton_drawScreen.isClicked((x,y)):
				print ' backbutton presed'
				self.screenMode= 'startScreen'

			elif self.helpButton_drawScreen.isClicked((x,y)):
				self.beforeHelpScreenMode=self.screenMode
				self.screenMode= 'helpScreen'

			elif self.undoButton_drawScreen.isClicked((x,y)):
				self.carPoly.undoLastPoint()

			elif self.discardButton_drawScreen.isClicked((x,y)):
				self.carPoly.pointList=[]

			else:
				self.carPoly.addPoint((x,y))

		elif self.screenMode == 'helpScreen':
			print 'here'

			# create two new buttons for navigation
			radius = 30
			cX,cY= 646,623
			nextButton=CircButton((cX,cY),radius)

			# create two new buttons for navigation
			radius = 30
			cX,cY= 378,623
			prevButton=CircButton((cX,cY),radius)

			index=self.currenthelpScrIndex
			print index
			print nextButton.isClicked((x,y)),'nextbutton'
			print prevButton.isClicked((x,y)),'prevButton'

			if index==0 and nextButton.isClicked((x,y)):
				self.currenthelpScrIndex=1
			elif index==2 and prevButton.isClicked((x,y)):
				self.currenthelpScrIndex=1
			elif index==1 and prevButton.isClicked((x,y)):
				self.currenthelpScrIndex=0
			elif index==1 and nextButton.isClicked((x,y)):
				self.currenthelpScrIndex=2
			elif self.backButton_drawScreen.isClicked((x,y)):
				self.screenMode=self.beforeHelpScreenMode
				self.beforeHelpScreenMode='startScreen'



		elif self.screenMode == 'layoutGeneratedScreen':
			if self.backButton_drawScreen.isClicked((x,y)):
				self.screenMode= 'startScreen'
			elif self.helpButton_drawScreen.isClicked((x,y)):
				self.beforeHelpScreenMode=self.screenMode
				self.screenMode='helpScreen'


	# creates a carpark layout based on data input.
	# currently just displays.
	# will need to refine.
	def generateDesign(self):
		""" this function does most computation job"""	

		print '\npointList = ', self.boundary.pointList


		pi_degrees=180
		(originX,originY)=self.boundary.getLayoutOrigin()
		orient=self.boundary.getLayoutOrientationDegrees()
		print 'origin x,y = ',(originX,originY)
		

		#orientRad=orient*math.pi/pi_degrees
		orientRad=math.radians(orient)

		print 'orientation of rotation = ', orient
		

		carLength=self.carPoly.getCarLength()
		carWidth=self.carPoly.getCarWidth()

		

		self.boundary.rotateAxis(-orientRad)
		print ' rotated boundary pointlist =',self.boundary.pointList

		boundExtentX= self.boundary.getMaxX()-self.boundary.getMinX()
		boundExtentY=self.boundary.getMaxY()-self.boundary.getMinY()
		print 'maxX, minX',self.boundary.getMaxX(),self.boundary.getMinX()
		print 'maxY,minY',self.boundary.getMaxY(),self.boundary.getMinY()

		self.boundary.rotateAxis(+orientRad)
		print 'unrotated pointList', self.boundary.pointList
		boundWidth=max(boundExtentY,boundExtentX)
		boundHeight=max(boundExtentX,boundExtentY)


		print "boundExtentX,boundExtentY,",boundExtentX,boundExtentY,\
			'\n carWidth=',carWidth, 'carLength=',carLength,'\n orientation',\
			 orient,'\n originX=',originX,'originY=',originY

		# need a carWidth, carLength, orientation, origin X, origin Y
		self.layout =CarParkLayout(boundExtentX,boundExtentY,
			carWidth=carWidth, carLength=carLength,orientation=orient,
			originX=originX,originY=originY)
		
	
	def run(self,width=1024,height=768):
		pygame.init()
		self.screen = pygame.display.set_mode((width,height))
		# self.screen2=pygame.display.set_mode((width,height))
		pygame.display.set_caption(self.title)

		self.loadImages()
		self.dataInit()

		while True:
			# self.clock.tick(1)
			# print self.clock
			self.timerFired()



def listFiles(path):
	# from class notes # cited!! :)
    if (os.path.isdir(path) == False):
        # base case:  not a folder, but a file, so return singleton list 
        # with its path
        return [path]
    else:
        # recursive case: it's a folder, return list of all paths
        files = [ ]
        for filename in os.listdir(path):
            files += listFiles(path + "/" + filename)
        return files





##########################################
### unused functions
##########################################



# inits some fixed parameters and could be used for debugging
	def debug_init(self):
		self.debug_carRotate=0
		self.debug_carScale=0.05
		self.debug_rowColTupList=[]
		self.debug_oWidth=50
		self.debug_oHeight=25
		for i in xrange(0,1024,self.debug_oWidth):
			for j in xrange(0,768,self.debug_oHeight):
				self.debug_rowColTupList.append((i,j))
		self.debug_h2=450
		self.debug_w2=300
		self.debug_carParkLayout=CarParkLayout(self.debug_w2,self.debug_h2,
			carWidth=25, carLength=50,orientation=0,originX=0,originY=0)

# unused function from anime class
def pixelPerfectCollisionDetection(self,sp1,sp2):
		"""
		Unused function.. but spent time understanding it.
		Internal method used for pixel perfect collision detection.
		citation : http://www.pygame.org/projects/9/207/
		only this function.
		"""
		rect1 = sp1.rect;     
		rect2 = sp2.rect;                            
		rect  = rect1.clip(rect2)
						
		hm1 = sp1.hitmask
		hm2 = sp2.hitmask
				
		x1 = rect.x-rect1.x
		y1 = rect.y-rect1.y
		x2 = rect.x-rect2.x
		y2 = rect.y-rect2.y

		for r in range(0,rect.height):      
			for c in range(0,rect.width):
				if hm1[c+x1][r+y1] & hm2[c+x2][r+y2]:
					return True

		return False


# move transformation for points to display 
# really because pygame takes NW corner point with respect to 
# screen and not surface
def transformCarPointsForDisplay(self):
	pi_deg=180.0
	three_half_pi=270.0

	d=self.carPointDict
	cW=self.carWidth
	cL=self.carLength
	O=self.orientationDeg

	for key in sorted(d):

		(x,y,discard)= d[key]

	
		# for angle in between 0 to 90 remains same,
		# So, No modification

		# if angle between 180 and 90 (90 incl. 180 excl.)
		if  O>=pi_deg/2.0 and O<pi_deg:
			a=(pi_deg-O)*math.pi/pi_deg
			# print key,'in 90,180'
			
			X=x-cW*math.cos(a)
			Y=y-cW*math.sin(a)
			d[key]=(X,Y,O)
			
		# if angle between 270 and 180 ( again 180 incl.)
		elif O>=pi_deg and O<three_half_pi:
			# print key, 'in 180,270'
			A= (O-pi_deg)*math.pi/pi_deg
			B= (three_half_pi- O)*math.pi/pi_deg

			

			X=x -cW*math.cos(A) -cL*math.cos(B)
			Y=y -cL*math.sin(B) +cW*math.sin(A)
			d[key]=(X,Y,O)

		# if angle between 270(incl. and 360)
		elif O<2*pi_deg and O>=three_half_pi:
			# print key, 'in 270,360'
			A = (O- three_half_pi) *math.pi/pi_deg
			X=x- cL*math.cos(A)
			Y=y- cW*math.sin(A)
			d[key]=(X,Y,O)

def unTransformCarPointsForDisplay(self):
	pi_deg=180.0
	three_half_pi=270.0

	d=self.carPointDict
	cW=self.carWidth
	cL=self.carLength
	O=self.orientationDeg

	for key in sorted(d):
		(x,y,discard)=d[key]
		
		# for angle in between 0 to 90 remains same,
		# So, No modification

		# if angle between 180 and 90 (90 incl. 180 excl.)
		if  O>=pi_deg/2.0 and O<pi_deg:
			a=(pi_deg-O)*math.pi/pi_deg
			X=x+cW*math.cos(a)
			Y=y+cW*math.sin(a)
			d[key]=(X,Y,O)
			
		# if angle between 270 and 180 ( again 180 incl.)
		elif O>=pi_deg and O<three_half_pi:
			A= (O-pi_deg)*math.pi/pi_deg
			B= (three_half_pi- O)*math.pi/pi_deg

			X=x +cW*math.cos(A) +cL*math.cos(B)
			Y=y +cL*math.sin(B) -cW*math.sin(A)
			d[key]=(X,Y,O)

		# if angle between 270(incl. and 360)
		elif O<2*pi_deg and O>=three_half_pi:
			A = (O- three_half_pi) *math.pi/pi_deg
			X=x+ cL*math.cos(A)
			Y=y+ cW*math.sin(A)
			d[key]=(X,Y,O)


# called from redrawAll. this function shall blit a carpark 
# layout onto a temp surface and then onto display.
# this is currently using debug_parameters
def blitCarParkLayout(self):

	# get a backdrop black in color
	someSurf=pygame.Surface((1024,768))
	someSurf.fill((255,255,255))
	self.screen.blit(someSurf,(0,0))

	# variables for actual test
	cpl=self.debug_carParkLayout
	d=self.debug_carParkLayout.carPointDict
	

	# scale the image
	self.carImg=pygame.transform.scale(self.carImg,(int(cpl.carWidth),
		int(cpl.carLength)))

	for key in d:
		x,y=d[key][0],d[key][1]
		self.carImg2=pygame.transform.rotate(self.carImg,d[key][2])
		self.screen.blit(self.carImg2,(int(x),int(y)))


# keyPressed with debugging modes.
# RIP.
def keyPressed(self):
	keyP=pygame.key.get_pressed()

	if keyP[K_ESCAPE]:
		pygame.quit()
		sys.exit()

	elif keyP[K_d]:
		self.debug_init()
		if self.screenMode=='debug_carParkLayout':
			self.screenMode= self.mode_before_debug
		else:
			self.mode_before_debug=self.screenMode
			self.screenMode= 'debug_carParkLayout'

	if self.screenMode == 'debug_parkingScheme':
		
		if keyP[K_UP]:
			self.debug_carScale +=0.01
		elif keyP[K_DOWN]:
			self.debug_carScale -= 0.01
		elif keyP[K_RIGHT]:
			self.debug_carRotate -= 5
		elif keyP[K_RIGHT]:
			self.debug_carRotate +=5
		elif keyP[K_w]:
			self.debug_oWidth+=3

			for i in xrange(0,1024,self.debug_oWidth):
				for j in xrange(0,768,self.debug_oHeight):
					self.debug_rowColTupList.append((i,j))

		elif keyP[K_s]:
			self.debug_oWidth -=3
			
			for i in xrange(0,1024,self.debug_oWidth):
				for j in xrange(0,768,self.debug_oHeight):
					self.debug_rowColTupList.append((i,j))
		
		elif keyP[K_a]:

			self.debug_oHeight -=3
			
			
			for i in xrange(0,1024,self.debug_oWidth):
				for j in xrange(0,768,self.debug_oHeight):
					self.debug_rowColTupList.append((i,j))

		elif keyP[K_x]:
			self.debug_oHeight +=3
			
			
			for i in xrange(0,1024,self.debug_oWidth):
				for j in xrange(0,768,self.debug_oHeight):
					self.debug_rowColTupList.append((i,j))


def test_setting1(self):

	# part A
	# failing for the following..
	# self.boundary.pointList=[(365, 540), (748, 201), (515, 164), (362, 437)]

	# self.carPoly.pointList=[(0,0),(0,50),(25,50),(25,0)]
	# self.boundary.pointList=[(196, 523), (781, 291), (510, 143), (255, 276)]

	# failing for the following point list..
	# self.boundary.pointList =  [(309, 284), (822, 596), (473, 627), (283, 466)]
	self.boundary.closePoly=True


# retired startscreen display function

def displayStartScreen(self):

		self.screen.blit(self.startScreen,(0,0))

		self.screen.blit(self.startScreenHeader,(0,0))




		# black translucent console surface
		# blackSurf=pygame.Surface([1024,110])



		# blackSurf.fill((0,0,0))
		# blackSurf.set_alpha(50)

		# self.screen.blit(blackSurf,(0,0))


		# left,top=self.loadImageButton.left,self.loadImageButton.top
		# self.screen.blit(self.loadImageButton_surf,(left,top))

		# left,top=self.drawButton.left,self.drawButton.top
		# self.screen.blit(self.drawButton_surf,(left,top))


		# left,top=self.drawCarButton.left,self.drawCarButton.top
		# self.screen.blit(self.drawCarButton_surf,(left,top))



		# left,top=self.homeButton.left,self.homeButton.top
		# self.screen.blit( self.homeButton_surf,(left,top))


		# left,top=self.helpButton.left,self.helpButton.top
		# self.screen.blit(self.helpButton_surf,(left,top))


		# left,top=self.helpButton.left,self.helpButton.top
		# self.screen.blit(self.computeButton_surf,(left,top))

# retirred start screen image load function

def loadStartScreenImages(self):
	pass
	self.startScreen = pygame.image.load(\
		"UI_Imgs/sampleParkingLotInt.jpg").convert()


	self.startScreen = pygame.Surface([1024,768])
	# fill it with out neutral yellow background
	self.startScreen.fill((249,237,232))

	self.startScreenHeader=pygame.image.load(\
		"UI_Imgs/topBannerForStartScreen.jpg").convert()
	self.startScreenHeader.set_alpha(100)


	# # load get image button
	# self.loadImageButton_surf=pygame.image.load(\
	# 	"UI_Imgs/loadImgButton.jpg").convert()
	# self.loadImageButton_surf.set_colorkey((0,0,0))
	# self.loadImageButton_surf.set_alpha(200)

	# # load draw poly button
	# self.drawButton_surf=pygame.image.load(\
	# 	"UI_Imgs/drawPolyButton.jpg").convert()
	# self.drawButton_surf.set_colorkey((0,0,0))
	# self.drawButton_surf.set_alpha(200)

	# # load draw car rect button
	# self.drawCarButton_surf=pygame.image.load(\
	# 	"UI_Imgs/carRectButton.jpg").convert()
	# self.drawCarButton_surf.set_colorkey((0,0,0))
	# self.drawCarButton_surf.set_alpha(200)

	# # load home button
	# self.homeButton_surf=pygame.image.load(\
	# 	"UI_Imgs/homeButton.jpg").convert()
	# self.homeButton_surf.set_colorkey((0,0,0))
	# self.homeButton_surf.set_alpha(200)

	# # load help button
	# self.helpButton_surf=pygame.image.load(\
	# 	"UI_Imgs/helpButton.jpg").convert()
	# self.helpButton_surf.set_colorkey((0,0,0))
	# self.helpButton_surf.set_alpha(200)

	# # load compute button
	# self.computeButton_surf=pygame.image.load(\
	# 	"UI_Imgs/computeButton.jpg").convert()
	# self.computeButton_surf.set_colorkey((0,0,0))
	# self.computeButton_surf.set_alpha(200)


### part of redrawAll function 

# elif self.screenMode== 'debug_parkingScheme':
# 			someSurf=pygame.Surface((1024,768))

# 			self.screen.blit(someSurf,(0,0))

# 			self.carImgScaled=pygame.transform.rotozoom(self.carImg,
# 				self.debug_carRotate, self.debug_carScale)
# 			self.carImgScaled.set_colorkey((255,255,255))

# 			for rowColTup in self.debug_rowColTupList:
# 				self.screen.blit(self.carImgScaled,rowColTup)
# 			pass


# small test code that can be placed to display translucent rectangle


# blah= pygame.Surface([100,200])
# blah.fill((20,20,20))
# blah.set_alpha(25)
# self.screen.blit(blah,(200,450))

##########################################
#test functions
##########################################




