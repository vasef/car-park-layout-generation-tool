import pygame
from pygame.locals import*
import math

def mousePressed(event, data):
    

    x = abs(data.mousePos[0] - data.centerX)
    y = abs(data.mousePos[1] - data.centerY)  
    
    distance = (x**2 + y**2)**0.5
    
    if distance < data.buttonRadius:
        (data.buttonMode, data.otherMode) = (data.otherMode, data.buttonMode)
    
    redrawAll(data)

def keyPressed(event, data):
    if event.key == pygame.K_q:
        data.mode = "Done"
    redrawAll(data)

def timerFired(data):
    redrawAll(data)
    data.clock.tick(20)
    data.rot+=1
    data.mousePos = pygame.mouse.get_pos()
    #manually manage the event queue
    for event in pygame.event.get():
        if (event.type == pygame.QUIT):
            pygame.quit()
            data.mode = "Done"
        elif (event.type == pygame.MOUSEBUTTONDOWN):
            mousePressed(event, data)
        elif (event.type == pygame.KEYDOWN):
            keyPressed(event,data)


def redrawAll(data):
    #analogous to canvas.delete(ALL)
    data.screen.fill(data.colorBlack)
    
    #actual drawing
    if (data.buttonMode == "off"):
        fillColor = data.colorRed
    else:
        fillColor = data.colorGreen
    
    radius = data.buttonRadius

    
    x0 = data.centerX - radius
    y0 = data.centerY - radius
    
    length = 2*radius
    height = 2*radius
    surfaction=pygame.image.load("UI_Imgs/carImg.jpg").convert()
    surfaction.set_colorkey((1,82,75))

    data.rec=Rect(100,100,-20,-20)
    print data.rec, 'before normalization'

    data.rec.normalize()
    print data.rec

    surfaction=pygame.transform.scale(surfaction,(20,50))


    # pygame.draw.rect(data.screen, fillColor, data.rec)
    surfaction= pygame.transform.rotate(surfaction,data.rot)
    # surfaction.fill((25,23,45,25))
    
    data.screen.blit(surfaction,(100,100))
    pygame.draw.rect(data.screen,fillColor,data.rec)
    print surfaction.get_rect()

    class Block(pygame.sprite.Sprite):

	    # Constructor. Pass in the color of the block,
	    # and its x and y position
	    def __init__(self, color, width=20, height=50,rotation=0):

	       # Call the parent class (Sprite) constructor
	       pygame.sprite.Sprite.__init__(self)

	       # Create an image of the block, and fill it with a color.
	       # This could also be an image loaded from the disk.
	       self.image = pygame.image.load("UI_Imgs/carImg.jpg").convert()
	       self.image.set_colorkey((1,82,75))
	       self.image= pygame.transform.scale(self.image,(width,height))
	       
	       # Fetch the rectangle object that has the dimensions of the image
	       # Update the position of this object by setting the values of rect.x and rect.y
	       self.rect = self.image.get_rect()

    
    #actually moves the drawing onto the screen
    pygame.display.flip()

def init(data):
    data.colorRed = (255, 0, 0)
    data.colorBlack = (0, 0, 0)
    data.colorGreen = (0, 255, 0)
    data.buttonMode = "off"
    data.otherMode = "on"
    data.buttonRadius = 60
    data.centerX = data.screenSize[0]/2
    data.centerY = data.screenSize[1]/2
    data.mode = "Running"

def run():
    pygame.init()
    
    #not given the Canvas class
    class Struct: pass
    data = Struct()
    
    #initialize the screen
    data.screenSize = (700,500)
    data.screen = pygame.display.set_mode(data.screenSize)
    pygame.display.set_caption("Game Window")
    
    
    #initialize clock
    data.clock = pygame.time.Clock()
    init(data)
    data.rot=0
    
    #recursion problems with timerFired
    while (data.mode != "Done"):
        timerFired(data)

    
    
run()